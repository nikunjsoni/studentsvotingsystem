DROP DATABASE IF EXISTS `poll`;
CREATE DATABASE IF NOT EXISTS `poll`;
use `poll`;
CREATE TABLE `candidate` (
  `cand_id` int(100) NOT NULL PRIMARY KEY,
  `fullname` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `about` varchar(255) NOT NULL,
  `votecount` int(255) NOT NULL DEFAULT 0
);

INSERT INTO `candidate` (`cand_id`, `fullname`, `position`, `about`, `votecount`) VALUES
(111503065, 'Akshay S', 'Gathering Secretary', 'Core team gathering 2017. Public relations head.', 0),
(111503019, 'Chirag J', 'Gathering Secretary', 'Finance head gathering. 2 Years experience. ', 0),
(111503034, 'Rohit K', 'ZEST Secretary', 'National level player. 2 Yeaars of experience in zest team.', 0),
(111503043, 'James B', 'ZEST Secretary', 'Won many state level awards.\r\nGood CGPA also.', 0);

CREATE TABLE `questions` (
  `qn_id` int(255) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `display_name` varchar(255) NOT NULL,
  `cand_id` int(10) NOT NULL,
  `qn` varchar(255) NOT NULL,
  `ans` varchar(255) NOT NULL DEFAULT 'Not Answered Yet'
);

INSERT INTO `questions` (`qn_id`, `display_name`, `cand_id`, `qn`, `ans`) VALUES
(5, 'ntt', 2, 'Question...\r\nWhat will you do if u get elected?', 'Answer...\r\nNothing much\r\nJust gonna chill'),
(6, 'nks', 4, 'Question...\r\nWhere do u live?', 'Not Answered Yet'),
(7, 'juk', 111503019, 'Question...\r\nWhat will you do if u win elections?', 'Answer...\r\nNothing much\r\nJust gonna chill'),
(8, 'nks', 111503034, 'Question...\r\nHow good are you in athletics?', 'Not Answered Yet');

CREATE TABLE `student` (
  `stud_id` varchar(255) NOT NULL UNIQUE,
  `email` varchar(128) NOT NULL UNIQUE,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL PRIMARY KEY,
  `course` varchar(100) NOT NULL,
  `yos` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'NOTVOTED'
);

INSERT INTO `student` (`stud_id`, `email`, `firstname`, `lastname`, `username`, `course`, `yos`, `status`) VALUES
('111503065', 'sonink15.comp@coep.ac.in', 'Nikunj', 'Soni', 'nks', 'BCS', '3', 'NOTVOTED'),
('111503037', 'kushwahaug15.comp@coep.ac.in', 'Utkarsh', 'Kushwaha', 'dante_', 'BCS', '3', 'NOTVOTED'),
('111503035', 'kulkarniju15.comp@coep.ac.in', 'Jay', 'kulkarni', 'juk', 'BCS', '3', 'NOTVOTED'),
('111503019', 'joshic15.comp@coep.ac.in', 'Chirag', 'J', 'cj', 'BCS', '3', 'NOTVOTED'),
('111503034', 'kholers15.comp@coep.ac.in', 'Rohit', 'Khole', 'rohit4603', 'BIT', '3', 'NOTVOTED');

CREATE TABLE `login` (
  `login_id` int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `rank` varchar(80) NOT NULL DEFAULT 'student',
  `status` varchar(10) NOT NULL DEFAULT 'ACTIVE'
);

INSERT INTO `login` (`username`, `password`, `rank`, `status`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'administrator', 'ACTIVE'),
('dante_', '6d15ce9fd7a85016907876466c36f353', 'candidate', 'ACTIVE'),
('juk', 'f053068af497b62021d33d6c12ad604c', 'student', 'ACTIVE'),
('nks', '25d55ad283aa400af464c76d713c07ad', 'student', 'ACTIVE'),
('cj', 'f1b8a1dc7bc92becb4d1ec38b25760d4', 'candidate', 'ACTIVE'),
('rohit4603', 'dc690178d000afd7c0ca40114481672d', 'candidate', 'ACTIVE');

