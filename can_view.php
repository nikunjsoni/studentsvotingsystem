
<?php 
include "globals.php";
session_start();
include "header_student.php";?>
<div id="cover">
<div id="content">
<h3>Candidate Profiles</h3>
<?php
include'connection.php';
	$member = $con->query("select * from candidate");
	if($member->num_rows == 0){
		echo '<font color="red" size = 10>No results found</font>';
	}
	else{
		echo '<table><tr bgcolor="#FF6600">
		<td width="100px">ID</td>		
		<td width="100px">FULLNAME</td>
		<td width="100px">POSITION</td>
		<td width="100px">ABOUT</td>';
		if($finishedElections == 1)
			echo'<td width="100px">VOTE</td>';
		echo '</tr>';
		while($mb = $member->fetch_object())
		{	
			$id = $mb->cand_id;
			$name = $mb->fullname;
			$pos = $mb->position;
			$about = $mb->about;
			$vote = $mb->votecount;
			echo '<tr bgcolor="#BBBEFF">';
			echo '<td>'.$id.'</td>';		
			echo '<td>'.$name.'</td>';
			echo '<td>'.$pos.'</td>';
			echo '<td>'.$about.'</td>';
			if($finishedElections == 1)
				echo '<td>'.$vote.'</td>';
			echo "</tr>";
		}
		echo'</table>';
	}
?>

</div>
</div>
</div>
<?php include "footer.php";?>
