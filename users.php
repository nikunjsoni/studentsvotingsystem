
<?php 
include "admin_auth.php";
session_start();
include "admin_header.php";?>
<div id="cover">
<div id="content">
<h4>Hello,&nbsp;<?php echo $_SESSION['SESS_NAME'] ;?><br /></h4>
<br/><h3 align = "center">Students:</h3>
<div id="login">
<?php
include'connection.php';
	$query = "SELECT * FROM student";
	$member = $con->query($query);
	if($member->num_rows == 0) {
		echo '<font color="red">No results found</font>';
	}
	else {
		echo '<table><tr bgcolor="#FF6600">
		<td width="100px">MIS</td>		
		<td width="100px">FIRSTNAME</td>
		<td width="100px">LASTNAME</td>
		<td width="100px">USERNAME</td>
		<td width="100px">COURSE</td>
		<td width="100px">YOS</td>
		<td width="100px">STATUS</td>
		<td width="100px">ACTION</td>
		</tr>';
	while($mb = $member->fetch_object()) {	
			$id = $mb->stud_id;
			$name = $mb->firstname;
			$name2 = $mb->lastname;
			$name3 = $mb->username;
			$course = $mb->course;
			$yos = $mb->yos;
			$status = $mb->status;
			echo '<tr bgcolor="#BBBEFF">';
			echo '<td>'.$id.'</td>';		
			echo '<td>'.$name.'</td>';
			echo '<td>'.$name2.'</td>';
			echo '<td>'.$name3.'</td>';
			echo '<td>'.$course.'</td>';
			echo '<td>'.$yos.'</td>';
			echo '<td>'.$status.'</td>';
			echo "<td><a href=delete.php?delete=".$id.">Delete</a></td>";
			echo "</tr>";
	}
	echo'</table>';
}
?>

<br/><h3>Candidates:</h3>
<?php
include'connection.php';
	$member = $con->query("select * from candidate");
	if($member->num_rows == 0){
		echo '<font color="red">No results found</font>';
	}
	else{
		echo '<table><tr bgcolor="#FF6600">
		<td width="100px">MIS</td>		
		<td width="100px">FULLNAME</td>
		<td width="100px">POSITION</td>
		<td width="100px">ABOUT</td>
		<td width="100px">VOTES</td>
		<td width="100px">ACTION</td>
		</tr>';
 		while($mb = $member->fetch_object()){	
			$id = $mb->cand_id;
			$name = $mb->fullname;
			$pos = $mb->position;
			$about = $mb->about;
			$vote = $mb->votecount;
			echo '<tr bgcolor="#BBBEFF">';
			echo '<td>'.$id.'</td>';		
			echo '<td>'.$name.'</td>';
			echo '<td>'.$pos.'</td>';
			echo '<td>'.$about.'</td>';
			echo '<td>'.$vote.'</td>';
			echo "<td width=2><a href=delete.php?delete=".$id.">Delete</a></td>";
			echo "</tr>";
		}
		echo'</table>';
	}
?>

<h3>Login users:</h3>
<?php
include'connection.php';
	$member = $con->query("select * from login");
	if($member->num_rows == 0) {
		echo '<font color="red">No results found</font>';
	}
	else{
		echo '<table><tr bgcolor="#FF6600">
		<td width="100px">ID</td>		
		<td width="100px">USERNAME</td>
		<td width="100px">RANK</td>
		<td width="100px">STATUS</td>
		<td width="100px">ACTION</td>
		<td width="100px">DEACTIVATE/ACTIVATE</td>
		</tr>';
		while($mb = $member->fetch_object()) {	
			$id = $mb->login_id;
			$name = $mb->username;
			$pos = $mb->rank;
			$about = $mb->status;
			echo '<tr bgcolor="#BBBEFF">';
			echo '<td>'.$id.'</td>';		
			echo '<td>'.$name.'</td>';
			echo '<td>'.$pos.'</td>';
			echo '<td>'.$about.'</td>';
			echo "<td><a href=delete.php?delete=".$name.">Delete</a></td>";
			echo "<td><a href=deactivate.php?deactivate=".$id.">Deactivate</a>/&nbsp;
			<a href=activate.php?activate=".$id.">Activate</a></td></tr>";
		}
		echo'</table>';
	}
?>

</div>
</div>
</div>
<?php include "footer.php";?>
